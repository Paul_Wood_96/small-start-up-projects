package paul.small.projects.kotlinbasics.functions

fun main() {
	val result = "Paul" to "Kotlin"

	println(result)


	/**
	 * Destructuring is limited to pairs but you can use it to break up
	 * pairs into individual parameters
	 */
	val (name, language) = "Paul" to "Kotlin"

	println(name)
}

/**
 * In any infix call the method name is placed in between the
 * target object name and it's parameter.
 */
infix fun Any.to(other: Any) = Pair(this, other)