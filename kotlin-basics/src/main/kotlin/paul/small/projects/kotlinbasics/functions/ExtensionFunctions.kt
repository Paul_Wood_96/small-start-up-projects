// changes the file class name so now to import the package you can use this file name
@file:JvmName("PlayingWithKotlinFunctions")

package paul.small.projects.kotlinbasics.functions

fun main() {
	// a function call with named arguments is like a default builder in java
	val result = listOf("Paul", "Cindy", "Grant").joinToString(separator = "#", postfix = "_")
	println(result)
}


fun String.lastChar(): Char {
	return this[length - 1]
}

// give the function call default values this is not accustomed in Java
fun <T> Collection<T>.joinToString(
		separator: String = ", ",
		prefix: String = "",
		postfix: String = ""
): String {
	val result = StringBuilder(prefix)

	// using a technique called kotlin destruction will explore it further in a bit
	for ((index, element) in this.withIndex()) {
		// only append a seperator after the first element
		if (index > 0) result.append(separator)
		result.append(element)
	}

	result.append(postfix);
	return result.toString()
}