package paul.small.projects.kotlinbasics.functions

class Person(val name: String, val surname: String)


fun main() {

	val person = Person("Paul", "Wood")
	person.validateBeforeSave()

}

/**
 * Localised function's make code cleaner to read and help structure your code
 * while also removing duplication
 *
 */
fun Person.validateBeforeSave() {
	fun validate(value: String, fieldName: String) {
		require(value = value.isNotEmpty()) {
			"Can not save unknown user missing field: $fieldName"
		}
	}
	validate(name, "Name")
	validate(surname, "Surname")
}