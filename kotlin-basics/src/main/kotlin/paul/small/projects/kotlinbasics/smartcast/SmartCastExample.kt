package paul.small.projects.kotlinbasics.smartcast

fun main() {
	outputType(5)
	outputType("Hello World!")
	outputType(Person("Paul", 23))
}

data class Person(val name: String, val age: Int)

fun outputType(x: Any) {
	when (x) {
		is Int -> println("Doubling the number: ${x * 2}")
		is String -> println("Outputting the message: $x")
		is Person -> println("Saying hello to: ${x.name}")
		else -> println("I dont know what else to really accomplish here")
	}
}
