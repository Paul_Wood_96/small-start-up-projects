package paul.small.projects.kotlinbasics.ranges

fun main() {
	val result = listOf(1..10)

	val result2 = listOf('a'..'f')

	result2.forEach {
		println("$it) Kotlin is fun")
	}

	result.forEach {
		print("$it ")
	}

	println()

	(1..10).forEach {
		println("$it) However its easy enough to just use ranges as lists")
	}
}

fun listOf(element: CharRange): List<Char> {
	return element.mapTo(ArrayList(), { c -> c })
}

fun listOf(element: IntRange): List<Int> {
	return element.mapTo(ArrayList(), { i -> i })
}