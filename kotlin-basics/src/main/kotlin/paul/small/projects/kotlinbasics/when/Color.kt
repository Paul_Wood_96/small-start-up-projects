package paul.small.projects.kotlinbasics.`when`

enum class Color(val r: Int, val g: Int, val b: Int) {
	RED(255, 0, 0),
	GREEN(0, 255, 0),
	YELLOW(255, 255, 0),
	ORANGE(255, 165, 0),
	// notice the semicolon is required only when declaring functions
	BLUE(0, 0, 255);

	fun rgb() =
			println("red: $r, green: $g, blue$b ")

}