package paul.small.projects.kotlinbasics.`when`

fun main() {
	println(getEmotionFromColour(Color.GREEN))
	println(getWarmth(Color.YELLOW))
	println(mixAndMatch(Color.RED, Color.YELLOW))
}

// when expresion must be exhaustive (you must declare all possible outcomes)
fun getEmotionFromColour(color: Color) =

		when (color) {
			// no break statements! this is expressive and assigns a value based of the response of the expression
			Color.RED -> "Anger"
			Color.BLUE -> "Happy"
			Color.GREEN -> "Envy"
			// or you can have a default branch and omit some colors
			else -> "Neutral"
		}

// and you can also combine some branches
fun getWarmth(color: Color) =
		when (color) {
			Color.GREEN, Color.BLUE -> "Cold"
			Color.YELLOW, Color.ORANGE -> "Warm"
			Color.RED -> "Red"
		}

// and when can be used with arbitrary objects as well
fun mixAndMatch(c1: Color, c2: Color) =
		when (setOf(c1, c2)) {
			setOf(Color.RED, Color.YELLOW) -> Color.ORANGE
			setOf(Color.BLUE, Color.YELLOW) -> Color.GREEN
			setOf(Color.BLUE, Color.BLUE) -> Color.BLUE
			// for the sake of brevity lets leave it at that
			else -> throw Exception("Not finished with the mappings")
		}

// whens with predicates
// in this case the else is explicit the condition becomes any Boolean expression
// highly simplified case don't think I like this implementation though
fun whenWithPredicate(value: Int) =
		when {
			(5 < value) -> "True"
			else -> "False"
		}
