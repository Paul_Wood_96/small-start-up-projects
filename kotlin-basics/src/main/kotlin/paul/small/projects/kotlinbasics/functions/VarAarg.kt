package paul.small.projects.kotlinbasics.functions

fun main() {
	val result = listOf("Paul", "Sarah", "Bob")

	/**
	 * Note that a list does not get split up by each element inside the list by default.
	 * Instead the only variable past here is a single list
	 */
	someFunctionWithVariableArguments(result)

	/**
	 * this function call makes varargs a little clearer. You can pass a variable
	 * number of elements to the function and the function will apply the same
	 * operation to each regardless of how many parameters where passed in
	 */
	someFunctionWithVariableArguments("Paul", "Sarah", "Bob")

}


fun <T> someFunctionWithVariableArguments(vararg values: T) {
	// values is an array of type T thanks to the vararg key word
	values.forEach {
		println("My name is $it")
	}
}
